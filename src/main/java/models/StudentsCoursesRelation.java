package models;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Optional;

@Entity
@Table(name = "students_courses")
public class StudentsCoursesRelation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "students_courses_id")
    private int studentsCoursesId;

    @Column(name = "student_pin")
    private String studentPin;

    @Column(name = "course_id")
    private int courseId;

    @Column(name = "completion_date")
    private LocalDate completionDate;

    public int getStudentsCoursesId() {
        return studentsCoursesId;
    }

    public void setStudentsCoursesId(int studentsCoursesId) {
        this.studentsCoursesId = studentsCoursesId;
    }

    public String getStudentPin() {
        return studentPin;
    }

    public void setStudentPin(String studentPin) {
        this.studentPin = studentPin;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }
}

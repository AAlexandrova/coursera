package repositories;

import models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import repositories.contracts.StudentsRepository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class StudentsRepositoryImpl implements StudentsRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public StudentsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Student> getStudentsForBenefits(int minCredit, LocalDate startDate, LocalDate endDate) {
        try (Session session = sessionFactory.openSession()) {
            Query<Student> query = session.createQuery(" select s from Student s " +
                    " inner join StudentsCoursesRelation sc on s.pin = sc.studentPin " +
                    " inner join Course c on c.courseId = sc.courseId " +
                    " where (sc.completionDate is not null) " +
                    " and (sc.completionDate > :startDate and sc.completionDate < :endDate) " +
                    " group by s.pin " +
                    " having ", Student.class);
            query.setParameter("startDate", startDate);
            query.setParameter("endDate", endDate);

            List<Student> result = query.list();
            if (result.size() == 0) {
                throw new IllegalArgumentException("No students eligible for benefits.");
            }
            return result;

/*
            select s.pin, s.first_name, s.last_name, sum(c.credit)
            from students s
            inner join students_courses sc on s.pin = sc.student_pin
            inner join courses c on sc.course_id = c.id
            where (sc.completion_date is not null)
            and sc.completion_date between '10.01.2022' and '10.02.2022'
            group by s.pin, s.first_name, s.last_name
            having sum(c.credit) > 1000

 */

        }
    }
}

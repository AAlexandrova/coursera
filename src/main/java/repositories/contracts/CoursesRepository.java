package repositories.contracts;
import models.Course;

import java.util.List;

public interface CoursesRepository {

    List<Course> getCoursesByStudent(String studentPin);
}

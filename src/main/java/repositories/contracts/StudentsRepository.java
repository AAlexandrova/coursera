package repositories.contracts;

import models.Student;

import java.time.LocalDate;
import java.util.List;

public interface StudentsRepository {
    List<Student> getStudentsForBenefits(int minCredit, LocalDate startDate, LocalDate endDate);
}

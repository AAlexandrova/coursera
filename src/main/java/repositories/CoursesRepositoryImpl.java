package repositories;

import models.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import repositories.contracts.CoursesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

@Repository
public class CoursesRepositoryImpl implements CoursesRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CoursesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Override
    public List<Course> getCoursesByStudent(String studentPin) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course c", Course.class);
            query.setParameter("studentPin", studentPin);

            List<Course> result = query.list();
            if (result.size() == 0) {
                throw new IllegalArgumentException("No courses of this student.");
            }
            return result;
        }

    }
}

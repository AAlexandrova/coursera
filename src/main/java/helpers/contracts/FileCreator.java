package helpers.contracts;

import java.io.File;

public interface FileCreator {

    File createFile();
}

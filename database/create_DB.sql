create or replace table instructors
(
    id           int auto_increment
        primary key,
    first_name   varchar(100) not null,
    last_name    varchar(100) not null,
    time_created datetime     not null
);

create or replace table courses
(
    id            int auto_increment
        primary key,
    name          varchar(150) not null,
    instructor_id int          not null,
    total_time    tinyint      not null,
    credit        tinyint      not null,
    time_created  datetime     not null,
    constraint courses_instructors__fk
        foreign key (instructor_id) references instructors (id)
);

create or replace table students
(
    pin          varchar(10) not null
        primary key,
    first_name   varchar(50) not null,
    last_name    varchar(50) not null,
    time_created datetime    not null
);

create or replace table students_courses
(
    student_pin         varchar(10) not null,
    course_id           int         not null,
    completion_date     date        null,
    students_courses_id int auto_increment
        primary key,
    constraint students_courses_courses__fk
        foreign key (course_id) references courses (id),
    constraint students_courses_students__fk
        foreign key (student_pin) references students (pin)
);

